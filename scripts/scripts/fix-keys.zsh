#!/usr/bin/zsh

# sudo pacman -Scc
# sudo pacman -Syy archlinux-keyring archlinux32-keyring archlinuxarm-keyring parabola-keyring
# sudo pacman-key --init
# sudo pacman-key --populate archlinux archlinux32 archlinuxarm parabola
# sudo pacman-key --refresh-keys

# If refresh-keys does not work use port 80! --keyserver hkp://sks-keyservers.net:80

# Upgrading keyring
# sudo pacman --needed -Syy archlinux-keyring parabola-keyring

# reset keys
# sudo reset-keys

sudo pacman -Scc
sudo pacman --needed -Syy archlinux-keyring parabola-keyring
sudo mv /etc/pacman.d/gnupg /etc/pacman.d/gnupg-bak
sudo pacman-key --init
sudo pacman-key --populate archlinux parabola
